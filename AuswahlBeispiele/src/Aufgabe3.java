import java.util.Scanner;	
public class Aufgabe3 {

	public static void main(String[] args) {
		
		int anzahlMäuse = 0;
		double GesamtPreis = 0;
			
		anzahlMäuse = anzahlMäuseErmitteln (anzahlMäuse);
		GesamtPreis = PreisErmitteln (anzahlMäuse, GesamtPreis);
		RechnungAusgabe(GesamtPreis);
	}
	
	public static int anzahlMäuseErmitteln (int anzahlMäuse) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Wie viele Mäuse bestellen Sie: ");
		anzahlMäuse = myScanner.nextInt();
		return anzahlMäuse;				
	}
	public static double PreisErmitteln(int anzahlMäuse, double GesamtPreis) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Wie viel kostet eine Maus?: ");
		double Preis = myScanner.nextDouble();
		if (anzahlMäuse < 10) {
			GesamtPreis = (anzahlMäuse*Preis)+10;
		}
		else {
			GesamtPreis = (anzahlMäuse*Preis);
		}
		return GesamtPreis;
	}
	public static void RechnungAusgabe (double GesamtPreis) {
		System.out.print("Der Gesamtbetrag beträgt: " + GesamtPreis);
	}
}
