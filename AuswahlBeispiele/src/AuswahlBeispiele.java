
public class AuswahlBeispiele {

	public static void main(String[] args) {
		
		int z1 = 5;
		int z2 = 7;
				
		int erg1 = min(z1,z2);
		System.out.println("Ergebnis(min) : "+ erg1);
		int erg2 = max(z1,z2);
		System.out.println("Ergebnis(max) : "+ erg2);
		double erg3 = max2(2.5,7.2,3.6);
		System.out.println("Ergebnis(max2) : "+ erg3);
	}
	
	public static int min (int zahl1, int zahl2) {
		int erg;
		if (zahl1 < zahl2) {
			erg = zahl1;
		}
		else {
			erg= zahl2;
		}
		return erg;
	}
	
	public static int max (int zahl1, int zahl2) {
		int erg;
		if (zahl1 > zahl2) {
			erg = zahl1;
		}
		else {
			erg= zahl2;
		}
		return erg;
	}
	public static double max2 (double zahl1, double zahl2, double zahl3) {
		double erg;
		if (zahl1 > zahl2 && zahl1 > zahl3) {
			erg = zahl1;
		}
		if (zahl2 > zahl1 && zahl2 > zahl3) {
			erg = zahl2;
		}
		else {
			erg = zahl3;
		}
		return erg;
	}
}



