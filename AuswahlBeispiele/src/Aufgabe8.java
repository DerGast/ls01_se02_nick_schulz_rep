import java.util.Scanner;	
public class Aufgabe8 {

	public static void main(String[] args) {
			
		int Jahreszahl = 0 ;
		boolean Schaltjahr;
		Jahreszahl = JahreszahlErfassen ();
		Schaltjahr =Regel1 (Jahreszahl);
		Schaltjahr = Regel2 (Jahreszahl, Schaltjahr);
		Schaltjahr = Regel3 (Jahreszahl, Schaltjahr);
		AusgabeErgebnis(Jahreszahl, Schaltjahr);
		
		

	}
	
	public static int JahreszahlErfassen () {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Welches Jahr soll �berpr�ft werden?: ");
		int Jahreszahl = myScanner.nextInt();
		return Jahreszahl;
	}
	
	public static boolean Regel1 (int Jahreszahl) {
		if (Jahreszahl % 4 == 0 ) {
			return true;
		}
		else {
			return false;	
		}
	}
		
	public static boolean Regel2 (int Jahreszahl, boolean Schaltjahr) {
		if (Schaltjahr == false) {
			return false;
		}
		if (Schaltjahr == true && Jahreszahl % 100 == 0) {
				return false;
		}	
		else {
				return true;
		}		
	}		
	public static boolean Regel3 (int Jahreszahl, boolean Schaltjahr) {
		if (Schaltjahr == true) {
			return true;
		}
		if (Schaltjahr == false && Jahreszahl % 400 == 0) {
			return true;
		}
		else {
			return false;
		}
	}
	public static void AusgabeErgebnis (int Jahreszahl, boolean Schaltjahr) {
		if (Schaltjahr == true) {
			System.out.print("Das Jahr " + Jahreszahl + " ist ein Schaltjahr.");
		}
		else {
			System.out.print("Das Jahr " + Jahreszahl + " ist kein Schaltjahr.");
		}
			
	}
		
}
