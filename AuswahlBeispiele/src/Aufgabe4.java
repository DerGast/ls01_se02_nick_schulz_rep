import java.util.Scanner;	
public class Aufgabe4 {
	public static void main(String[] args) {
		
		double Bestellwert = 0;
		double Rabatt = 0;
		double Gesamtpreis = 0;
		
		Bestellwert = BestellungErfassen(Bestellwert);
		Rabatt = BerechnungRabatt(Bestellwert, Rabatt);
		Gesamtpreis = BerechnungPreis(Bestellwert, Rabatt, Gesamtpreis);
		System.out.print("Der Gesamtbetrag inklusive " + (100 - Rabatt) + (" % Rabatt und 19% Mehrwertsteuer betr�gt:") + Gesamtpreis + "�");
		
	}
	
	public static double BestellungErfassen (double Bestellwert) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Wie hoch ist Ihr Bestellwert?: ");
		Bestellwert = myScanner.nextDouble();
		return Bestellwert;
	}
	public static double BerechnungRabatt (double Bestellwert, double Rabatt) {
		if (Bestellwert > 0 && Bestellwert <= 100) {
			return  90;
		}
		if(Bestellwert>100 && Bestellwert <= 500) {
			return 85;
		}
		else	{
			return 80;
		}				
		
	}
	public static double BerechnungPreis (double Bestellwert, double Rabatt, double Gesamtpreis) {
		double PreisOhneMwSt = (Bestellwert * Rabatt) / 100;
		Gesamtpreis = (PreisOhneMwSt * 119) / 100;
		return Gesamtpreis;
	}
	
}
