import java.util.Scanner;
public class SchleifenAufgabe5 {

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie die Laufzeit in Jahren ein: ");
		double jahre = tastatur.nextDouble();;
		System.out.println("Geben Sie Ihr Kapital in Euro an: ");
		double akapital = tastatur.nextDouble();
		System.out.println("Geben Sie den Zinssatz in % an: ");
		double zinssatz = tastatur.nextDouble();
		double zaehler = 0;
		double ekapital = akapital;
		
		while (zaehler < jahre) {
			ekapital = ekapital * (100 + zinssatz) / 100;
			zaehler ++;
		}		
		System.out.println("Eingezahltes Kapital: " + akapital + "�");
		System.out.printf("Ausgezahltes Kapital: %.2f�",ekapital);
	}

}
