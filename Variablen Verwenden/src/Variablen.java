﻿

/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	  	short anzahlProgrammdurchlaeufe;
    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  	anzahlProgrammdurchlaeufe = 25;
	  	System.out.println("Anzahl der Programmdurchläufe: " + anzahlProgrammdurchlaeufe);
	  	
    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */
	  	char  eingabeBuchstabe;
    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  	eingabeBuchstabe = 'C';
	  	System.out.println("Der eingegebene Buchstabe ist: " + eingabeBuchstabe);
    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	  	long astronomischeZahlenwerte;
    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	  	astronomischeZahlenwerte = 299792 ;
	  	System.out.println("Die Lichtgeschwindigkeit beträgt " + astronomischeZahlenwerte + " km pro Sekunde");
    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	  	short anzahlMitglieder = 10000;
    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	  	System.out.println("Anzahl der Mitglieder: " + anzahlMitglieder);
    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	  	double elektrischeElementarladung = -1.602E-19;
	  	System.out.println("Elektrische Elementarladung: " + elektrischeElementarladung + " As");
    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	  	boolean hatbezahlt;
    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	  	hatbezahlt = true;
	  	System.out.println("Die Zahlung ist erfolgt: " + hatbezahlt );

  }//main
}// Variablen