import java.util.Scanner;
public class SchleifenAufgabe1 {

	public static void main(String[] args) {
		
		aufgabeA();
		aufgabeB();
		
	}
		
		
		//Methode Aufgabe A
	public static void aufgabeA() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie bitte eine Zahl n ein:");
		int n = tastatur.nextInt();
		int zaehler = 1;
		
		if (n >= 0)		
		while (n>=zaehler) {
			if (zaehler==n) {			
			System.out.print(zaehler + " ");
			}
			else {
			System.out.print(zaehler + ", ");
			}
			zaehler++;
		}	
		
	 }
		//Methode Aufgabe B
	public static void aufgabeB() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("\n\nGeben Sie bitte eine Zahl n ein:");
		int n = tastatur.nextInt();
		int zaehler = 1;
	
		if (n >= 0)		
			while (n>=zaehler) {
				if (zaehler==n) {			
					System.out.print(n + " ");
				}
				else {
					System.out.print(n + ", ");
				}
				n--;
			}	
	
	}			
}
