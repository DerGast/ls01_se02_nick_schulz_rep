import java.util.ArrayList;
public class RaumschiffTest {
	public static void main(String[] args) {
		
		Raumschiff r1 = new Raumschiff();
		 r1.setPhotonentorpedoAnzahl(1);
		 r1.setEnergieversorgungInProzent(100);
		 r1.setSchildeInProzent(100);
		 r1.setHuelleInProzent(100);
		 r1.setLebendserhaltungssystemeInProzent(100);
		 r1.setAndroidenAnzahl(2);
		 r1.setSchiffsname("IKS Hegh'ta");
		Raumschiff r2 = new Raumschiff( 2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff r3 = new Raumschiff( 0, 80, 80, 50, 100, 5, "Ni'Var");		
		
		raumschiffZustaendeTest(r1, r2, r3);
		addLadungTest(r1, r2, r3);
		ladungAusgebenTest(r1, r2, r3);
		torpedoSchiessenTest(r1,r2,r3);
		raumschiffZustaendeTest(r1, r2, r3);
	}
	
	public static void raumschiffZustaendeTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r1.raumschiffZustaende();
		r2.raumschiffZustaende();
		r3.raumschiffZustaende();
	}
	
	public static void addLadungTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r1.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		r1.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));
		
		r2.addLadung(new Ladung("Borg-Schrott", 5));
		r2.addLadung(new Ladung("Rote Materie", 2));
		r2.addLadung(new Ladung("Plasma-Waffe", 50));
		
		r3.addLadung(new Ladung("Forschungssonde", 35));
		r3.addLadung(new Ladung("Photonentorpedo", 3));
		
	}	
	
	public static void torpedoSchiessenTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r1.photonentorpedoSchiessen(r2);
		r2.photonentorpedoSchiessen(r3);
		r3.photonentorpedoSchiessen(r1);
	}
	
	public static void phaserSchiessenTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r1.phaserkanoneSchiessen(r2);
		r2.phaserkanoneSchiessen(r3);
		r3.phaserkanoneSchiessen(r1);
	}
	
	public static void ladungAusgebenTest(Raumschiff r1, Raumschiff r2, Raumschiff r3) {
		r1.ladungAusgeben();
		r2.ladungAusgeben();
		r3.ladungAusgeben();
	}
}
