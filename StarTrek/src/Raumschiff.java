import java.util.ArrayList;
import java.util.Scanner;
public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebendserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
		
	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorgungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent  = 0;
		this.lebendserhaltungssystemeInProzent  = 0;
		this.androidenAnzahl  = 0;
		this.schiffsname = "Unbekannt";
		this.ladungsverzeichnis = new ArrayList<>();
		this.broadcastKommunikator = new ArrayList<>();
	}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebendserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent  = huelleInProzent;
		this.lebendserhaltungssystemeInProzent  = lebendserhaltungssystemeInProzent;
		this.androidenAnzahl  = androidenAnzahl;
		this.schiffsname = schiffsname;	
	}
	
	public void setPhotonentorpedoAnzahl (int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	
	public void setEnergieversorgungInProzent (int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	
	public void setSchildeInProzent (int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	
	public void setHuelleInProzent (int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	
	public void setLebendserhaltungssystemeInProzent (int lebendserhaltungssystemeInProzent) {
		this.lebendserhaltungssystemeInProzent = lebendserhaltungssystemeInProzent;
	}
	public int getLebendserhaltungssystemeInProzent() {
		return lebendserhaltungssystemeInProzent;
	}
	
	public void setAndroidenAnzahl (int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}
	
	public void setSchiffsname (String schiffsname) {
		this.schiffsname = schiffsname;
	}
	public String getSchiffsname() {
		return schiffsname;
	}
	
	public void addLadung (Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
		System.out.println("Ladung hinzugef�gt!");
	}
	
	public void raumschiffZustaende () {
		System.out.println("Schiffsname: " + getSchiffsname());	
		System.out.println("Photonentorpedo: "+getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung: " + getEnergieversorgungInProzent() + "%");
		System.out.println("Schilde: " + getSchildeInProzent() + "%");
		System.out.println("H�lle: " + getHuelleInProzent() + "%");
		System.out.println("Lebendserhaltungssysteme: " + getLebendserhaltungssystemeInProzent() + "%");
		System.out.println("Androiden: " + getAndroidenAnzahl());	
		System.out.println("");
	}
	
	public void ladungAusgeben() {
		System.out.println("");
		System.out.println("Ladungsliste:" );
		for (Ladung ladung : ladungsverzeichnis) {
			System.out.println(" " + ladung);
		}
		System.out.println("");
	}
	
	public void photonentorpedoSchiessen (Raumschiff Verteidiger) {
		if (photonentorpedoAnzahl < 1) {
			System.out.println("Keine Photonentorpedos gefunden!");
			System.out.println("");
			nachrichtAnAlle("-=*click*=-");		
		}
		else {
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			nachrichtAnAlle("Photonenorpedo abgeschossen!");
			Verteidiger.treffer();
		}
	}		
	
	public void phaserkanoneSchiessen (Raumschiff Verteidiger) {	
		if (energieversorgungInProzent >= 50) {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			nachrichtAnAlle("Phaserkanone abgeschossen!");
			Verteidiger.treffer();
		}
		else {
			nachrichtAnAlle("-=*click*=-");
		}	
	}
	
	private void treffer () {
		if (getHuelleInProzent() <= 0) {
			System.out.println("Raumschiff bereits zerst�rt!");
		}
		else {
			System.out.println(getSchiffsname() + "wurde getroffen!");	
			System.out.println("");
			if (getSchildeInProzent() > 0) {
				setSchildeInProzent(getSchildeInProzent() - 50);
			}
			else if (getSchildeInProzent() <= 0 && getHuelleInProzent() >0){
				setHuelleInProzent(getHuelleInProzent() -50);
				setEnergieversorgungInProzent(getEnergieversorgungInProzent() -50);
			}
			if (getHuelleInProzent() <= 0) {
				setLebendserhaltungssystemeInProzent(0);
				nachrichtAnAlle("Die Lebendserhaltungssysteme wurden Zerst�rt!");
			}
		}	
	}
	
	public void nachrichtAnAlle (String message) {	
		broadcastKommunikator.add(message);
	}
	public void eintraegeLogbuchZurueckgeben() {	
		System.out.println(broadcastKommunikator);
	}
}
	
	

	
