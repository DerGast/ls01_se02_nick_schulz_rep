import java.util.ArrayList;

public class LadungTest {
	public static void main(String[] args) {
		
		Ladung l1 = new Ladung();
		l1.setBezeichnung("Ferengi Schneckensaft");
		l1.setMenge(200);
		System.out.println("Ladung l1:");
		System.out.println(l1.getBezeichnung());
		System.out.println(l1.getMenge());
		System.out.println();
		
		Ladung l2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		System.out.println("Ladung l2:");
		System.out.println(l2.getBezeichnung());
		System.out.println(l2.getMenge());
		System.out.println();
		
		Ladung l3 = new Ladung("Borg-Schrott", 5);
		System.out.println("Ladung l3:");
		System.out.println(l3.getBezeichnung());
		System.out.println(l3.getMenge());
		System.out.println();
		
		Ladung l4 = new Ladung("Rote Materie",2);
		System.out.println("Ladung l4:");
		System.out.println(l4.getBezeichnung());
		System.out.println(l4.getMenge());
		System.out.println();
		
		Ladung l5 = new Ladung("Plasma-Waffe", 50);
		System.out.println("Ladung l5:");
		System.out.println(l5.getBezeichnung());
		System.out.println(l5.getMenge());
		System.out.println();
		
		Ladung l6 = new Ladung("Forschungssonde", 35);
		System.out.println("Ladung l6:");
		System.out.println(l6.getBezeichnung());
		System.out.println(l6.getMenge());
		System.out.println();
		
		Ladung l7 = new Ladung("Photonentorpedo", 3);
		System.out.println("Ladung l7:");
		System.out.println(l7.getBezeichnung());
		System.out.println(l7.getMenge());
		System.out.println();
		
		ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
		ladungsverzeichnis.add(l1);
			}
	}

