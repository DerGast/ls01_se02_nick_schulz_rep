package Auftrag3;

 /* Arbeitsauftrag:  Lesen Sie alle Angaben des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                    titel:  Java ist auch eine Insel   
 *					  vorname:  Christian 
 *                    nachname:  Ullenboom 
 */

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData3 {

	public static void main(String[] args) {

		try {
			// Name der Datei: "src/Auftrag3/buchhandlung.xml"
			// Add your code here
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("src/Auftrag3/buchhandlung.xml");
			
			NodeList titelListe = doc.getElementsByTagName("titel");
			Node titel = titelListe.item(0);
			NodeList vornameListe = doc.getElementsByTagName("vorname");
			Node vorname = vornameListe.item(0);
			NodeList nachnameListe = doc.getElementsByTagName("nachname");
			Node nachname = nachnameListe.item(0);
			
			
			
			System.out.println("Titel: " + titel.getTextContent());
			System.out.println("Autor: ");
			System.out.println("  Vorname: " + vorname.getTextContent());
			System.out.println("  Nachname: " + nachname.getTextContent());
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

}
