package Auftrag5;

import javax.xml.parsers.*;
import org.w3c.dom.*;

 /* Arbeitsauftrag:  Lesen Sie nur die Autoren des Buches "XQuery Kick Start" aus der Datei  
 *                  "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *                  
 *                   Ausgabe soll wie folgt aussehen:
 *                     Buchtitel:  XQuery Kick Start
 *                     Autoren: 
 *                     	    1. autor: James McGovern
 *                          2. autor: Per Bothner
 *                          3. autor: Kurt Cagle
 *                          4. autor: James Linn
 *                          5. autor: Vaidyanathan Nagarajan
 *                          
 * Hinweis: Sie ben�tigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */



public class ReadBookstoreData5 {

	public static void main(String[] args) {
		
		try {

			// Name der Datei: "src/Auftrag5/buchhandlung.xml"
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("src/Auftrag5/buchhandlung.xml");
			
			NodeList autorListe = doc.getElementsByTagName("autor");
			NodeList titelListe = doc.getElementsByTagName("titel");
			Node titel  = titelListe.item(2);
			System.out.println("Buchtitel: " + titel.getTextContent());
			
			for (int z = 2; z < 7; z ++) {
				Node autor = autorListe.item(z);
				System.out.println("Autor: " + autor.getTextContent());
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}


	}

}
