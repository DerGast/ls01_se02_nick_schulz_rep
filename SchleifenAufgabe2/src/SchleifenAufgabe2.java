import java.util.Scanner;

public class SchleifenAufgabe2 {
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben Sie bitte eine Zahl n ein:");
		int eingabeZahl = tastatur.nextInt();
		long ergebniss = 1;
		int zaehler = 1;
	
		while (zaehler <= eingabeZahl) {
			System.out.print(zaehler);
			if (zaehler == eingabeZahl) {
				System.out.print(" = ");		
			}			
			else {
				System.out.print(" * ");					
			}		
			ergebniss = ergebniss * zaehler;
			zaehler++;
		}
			System.out.print(ergebniss);
			
	}
}


	
