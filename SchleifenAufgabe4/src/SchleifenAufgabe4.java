import java.util.Scanner;
public class SchleifenAufgabe4 {
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben Sie Ihren Startwert ein: ");
		double startwert = tastatur.nextDouble();
		System.out.print("Geben Sie Ihren Endwert ein: ");
		double endwert = tastatur.nextDouble();
		System.out.print("Geben Sie Ihre Schrittweite ein: ");
		double schrittweite = tastatur.nextDouble();
		double cwert = startwert;
		double fwert = 0;
		
		
		while (cwert <= endwert) {
			fwert = cwert * 1.8 + 32;
			System.out.printf("%.2f�C          %.2f�F%n",cwert, fwert);
			cwert = cwert + schrittweite;
		}
	}
	

}
