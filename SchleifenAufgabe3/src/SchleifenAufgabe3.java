import java.util.Scanner;
public class SchleifenAufgabe3 {
	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Geben Sie Ihre Zahl ein: ");
		int zahl = tastatur.nextInt();
		int summe = 0;
		
		while (0 != zahl) {
			summe = summe + (zahl % 10);
			zahl = zahl / 10;
		}
		System.out.println(""+summe);
		
		
	}
}
