package Auftrag2;
/*
    {
		"titel": "Java ist auch eine Insel",
		"jahr": 1998,
		"preis": 29.95,
		"autor": "Christian Ullenboom"
	}
*/
import java.io.*;
import javax.json.*;
import javax.json.spi.*;

public class WriteBookDataJason {

	public static void main(String[] args) {
		
		Buch b1 = new Buch("Java ist auch eine Insel", 1998, 29.95, "Christian Ullenboom");
		
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("titel", b1.getTitel());
        builder.add("jahr", b1.getJahr());
        builder.add("preis", b1.getPreis());
        builder.add("autor", b1.getAutor());
        
        JsonObject jo = builder.build();
        
        try {
			FileWriter fw = new FileWriter("book2.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
 
        
	}

}
