package Auftrag3;

public class Buch {
	private String name;
	private String tel;
	private String fax;
	private String strasse;
	private int plz;
	private String hausnummer;
	private String ort;

	public Buch(String name, String tel, String fax, String strasse, String hausnummer, int plz, String ort) {
		this.name = name;
		this.tel = tel;
		this.fax = fax;
		this.strasse = strasse;
		this.hausnummer = hausnummer;
		this.plz = plz;
		this.ort = ort;
	}

	public String getName() {
		return name;
	}

	public String getTel() {
		return tel;
	}

	public String getFax() {
		return fax;
	}
	
	public String getStrasse() {
		return strasse;
	}
	
	public int getPlz() {
		return plz;
	}
	
	public String getHausnummer() {
		return hausnummer;
	}
	
	public String getOrt() {
		return ort;
	}



	
}
