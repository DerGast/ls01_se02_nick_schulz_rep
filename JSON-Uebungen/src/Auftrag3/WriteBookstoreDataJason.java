package Auftrag3;

/*
    {
		"name": "OSZIMT Buchhandlung",
		"tel": "030-225027-800",
		"fax": "030-225027-809",
		"adresse": {
			"strasse": "Haarlemer Straße",
			"hausnummer": "23-27",
			"plz": "12359",
			"ort": "Berlin"
		}
	}

*/
import java.util.*;
import java.io.*;
import javax.json.*;
import javax.json.spi.*;


public class WriteBookstoreDataJason {

	public static void main(String[] args) {

		Buch buch = new Buch("OSZIMT Buchhandlung", "030-225027-800", "030-225027-809", "Haarlemer Stra�e", "23-27",
				12359, "Berlin");

		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("name", buch.getName());
		builder.add("tel", buch.getTel());
		builder.add("fax", buch.getFax());

		JsonObjectBuilder adresse = Json.createObjectBuilder();
		adresse.add("strasse", buch.getStrasse());
		adresse.add("hausnummer", buch.getHausnummer());
		adresse.add("plz", buch.getPlz());
		adresse.add("ort", buch.getOrt());
		
		builder.add("adresse",adresse);

		JsonObject jo = builder.build();

		try {
			FileWriter fw = new FileWriter("book3.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();
		} catch (Exception ex) {
			ex.printStackTrace();

		}

	}
}
