package Auftrag1a;
/*
    {
    	"titel" : "XQuery Kick Start"
    }
*/
import java.io.*;
import java.util.ArrayList;

import javax.json.*;
import javax.json.spi.*;


public class WriteBookDataJason {

	public static void main(String[] args) {
		
	    Buch buch = new Buch("XQuery Kick Start");
		
		JsonObjectBuilder  builder = Json.createObjectBuilder();
		builder.add("titel", buch.getTitel());	
	    
		JsonObject jo = builder.build();
		
		try {
			FileWriter fw = new FileWriter("book1.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}

	}

}
