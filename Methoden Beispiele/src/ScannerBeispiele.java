import java.util.Scanner;								//1.Schritt Importieren
public class ScannerBeispiele {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner (System.in);	//2.Schritt Objekt erstellen
	
		double Zahl = myScanner.nextDouble();			//3.Schritt
		
		boolean b = myScanner.nextBoolean();
	}
	

}
