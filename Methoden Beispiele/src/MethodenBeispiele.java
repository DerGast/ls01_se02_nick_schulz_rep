import java.util.Scanner;

public class MethodenBeispiele {

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner (System.in);
			
		String vname = leseString("Geben Sie bitte Ihren Voramen ein:");
		String nname = leseString("Geben Sie bitte Ihren Nachnamen ein:");
		sayHello(vname, nname);
		
		int alter = leseInt ("Geben Sie bitte Ihr Alter ein:");
			System.out.println("Alter: " + alter);

	}
	
	public static String leseString(String text) {
			System.out.println(text);
			Scanner myScanner = new Scanner(System.in);
			String str = myScanner.next();
			return str;		
	}

 	public static int leseInt(String text) {
			System.out.println(text );
			Scanner myScanner = new Scanner(System.in);		
			int Int = myScanner.nextInt();		
			return Int;
}
	
	public static void sayHello(String vname, String nname) {
		System.out.println("What's up " + vname + " " + nname);
	}
	
	public static void sayHello() {
		System.out.println("hello Max");
	}

}
