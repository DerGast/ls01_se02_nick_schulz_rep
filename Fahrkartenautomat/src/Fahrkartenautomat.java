﻿import java.util.Scanner;
class Fahrkartenautomat{
    public static void main(String[] args){
    	
    	
       double zuZahlenderBetragTicket;
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       double zuZahlenderBetrag; 

       
       
       zuZahlenderBetrag = fahrkartenbestellungErfassenTeil1();
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(eingezahlterGesamtbetrag,zuZahlenderBetrag);
       wiederholung();
                                
    }
   // 1. Methode ticketpreis
   public static double fahrkartenbestellungErfassenTeil1() {
    	double preis = 0;
    	double betrag = 0;
    	int anzahl = 0;
    	double [] fahrkartenpreis = {2.9,3.3,3.6,1.9,8.6,9,9.6,23.5,24.3,24.9};
    	String [] fahrkarten = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC","Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};   
    	
    	System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin aus:");
    	
    	for (int i = 0 ; i < fahrkarten.length ; i++) {
    		int u = i + 1;
			
			System.out.printf("	Für " + fahrkarten[i] + " [%.2f€] wählen Sie die " + u + "%n", fahrkartenpreis[i]);
    	}
    	 
     	 Scanner tastatur = new Scanner(System.in);
     	 int karte  = tastatur.nextInt();  	 		
    	 
    	 if (karte < 1 || karte > 10 ){
    		 System.out.print("Ihre Wahl: "+ karte + "\n");
    		 System.out.print("\n>>>falsche Eingabe<<<\n\n");
    		 fahrkartenbestellungErfassenTeil1();
    		 System.out.print("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus:\n");
    	 }
    	   	    
    	 else 
    		 System.out.printf("Ihre Wahl: "+ karte + ", " + fahrkarten[karte-1] +" für %.2f pro Karte.\n", fahrkartenpreis[karte-1]);
    		 preis = fahrkartenpreis[karte - 1];    		 
    		 System.out.print("\nAnzahl der Tickets eingeben (MAX. 10): \n");
    		 anzahl = tastatur.nextInt();            
    	     while (anzahl < 1 || anzahl > 10) {	 
    	    		System.out.print("\nAnzahl der Tickets erneut eingeben (MAX. 10): \n");
    	   		 	anzahl = tastatur.nextInt();
    	     }
    		 betrag = anzahl * (preis*100);
             return betrag;                               	       	
    }

   
    //2.Methode Bezahlung
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {   
    	
    	double eingezahlterGesamtbetrag = 0.0;
		double rückgabebetrag = 0.0;
		double eingeworfeneMünze;
		double geldmittel [] = {0.05, 0.10, 0.20, 0.50, 1, 2};
		int pruefung = 0;
		
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
    		Scanner tastatur = new Scanner(System.in); 
    		System.out.printf("\nNoch zu zahlen: %.2f Euro%n",(zuZahlenderBetrag - eingezahlterGesamtbetrag) / 100);
    		System.out.println("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    		eingeworfeneMünze = tastatur.nextDouble();
    		
    		for (int i = 0; i < geldmittel.length; i++) {
    			if (geldmittel [i] != eingeworfeneMünze) {
    				pruefung = pruefung+1;
    			}
    		}
    		if (pruefung == 5) {
    			eingezahlterGesamtbetrag += eingeworfeneMünze*100;
    			pruefung = 0;
    		}
    		else
    			System.out.println("****Bitte gültige Geldmittel einwerfen!****");
    	} 
    	return eingezahlterGesamtbetrag;
    }
    
    //3.Methode Fahrkartenausgabe
    public static void fahrkartenAusgeben() {
    
    	System.out.println("\nFahrschein wird ausgegeben");
    	for (int i = 0; i < 26; i++)
    	{
    		System.out.print("=");
    		try {
    			Thread.sleep(10);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}
    	System.out.println("\n\n");  
		
    }
    
    //4.Methode Rückgeld ausgeben
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	
    	double rückgabebetrag = 0;
    	
        rückgabebetrag = (eingezahlterGesamtbetrag - zuZahlenderBetrag) / 100;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO%n",(rückgabebetrag));
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("   * * *");
         	  System.out.println(" *       *");
         	  System.out.println("*    2    *");
         	  System.out.println("*   EURO  *");
         	  System.out.println(" *       *");
         	  System.out.println("   * * *");
 	          rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
              System.out.println("   * * *");
           	  System.out.println(" *       *");
           	  System.out.println("*    1    *");
           	  System.out.println("*   EURO  *");
           	  System.out.println(" *       *");
           	  System.out.println("   * * *");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
            {
              System.out.println("    ***");
           	  System.out.println(" *       *");
           	  System.out.println("*    50   *");
           	  System.out.println("*   CENT  *");
           	  System.out.println(" *       *");
           	  System.out.println("   * * *");
 	          rückgabebetrag -= 0.50;
            }
            while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
            {
            	  System.out.println("   * * *");
             	  System.out.println(" *       *");
             	  System.out.println("*    20   *");
             	  System.out.println("*   CENT  *");
             	  System.out.println(" *       *");
             	  System.out.println("   * * *");
   	          rückgabebetrag -= 0.50;
  	          rückgabebetrag -= 0.20;
            }
            while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
            {
            	  System.out.println("   * * *");
             	  System.out.println(" *       *");
             	  System.out.println("*    10   *");
             	  System.out.println("*   CENT  *");
             	  System.out.println(" *       *");
             	  System.out.println("   * * *");
   	          rückgabebetrag -= 0.50;
 	          rückgabebetrag -= 0.10;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
            	  System.out.println("   * * *");
             	  System.out.println(" *       *");
             	  System.out.println("*    5    *");
             	  System.out.println("*   CENT  *");
             	  System.out.println(" *       *");
             	  System.out.println("   * * *");
   	          rückgabebetrag -= 0.50;
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
    }
    //Programm neustarten
    public static void wiederholung() {
    	Scanner tastatur = new Scanner(System.in);
    	System.out.print("\nWollen Sie noch mehr Fahrkarten kaufen? [Ja/Nein]\n\n");
    	String antwort = tastatur.next();
    	if (antwort.equals("JA") || antwort.equals("ja") || antwort.equals("Ja") || antwort.equals("j")) {
    		fahrkartenbestellungErfassenTeil1();
    	}
    	else 
    		System.out.print("\nBesuchen Sie uns bald wieder!\n");
    		System.exit(1);
    	
    }
}



    	
